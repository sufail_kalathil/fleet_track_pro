import { Button, Col, Row, Toast } from "react-bootstrap";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { closeToast, showFailToast } from "../store/Toast/actions";

export const ToastBar = () => {
  const data = useSelector((state: any) => state.toastHandler);
  const dispatch = useDispatch();

  const onClose = () => {
    dispatch(closeToast());
  };

  return (
    <Row>
      <Col xs={6}>
        <Toast onClose={onClose} show={data.show} delay={1000} autohide>
          <Toast.Header>
            <strong className="me-auto">{data.title}</strong>
          </Toast.Header>
          <Toast.Body>{data.message}</Toast.Body>
        </Toast>
      </Col>
    </Row>
  );
};
