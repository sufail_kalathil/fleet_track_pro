import Table from "react-bootstrap/Table";

export const DataTable = ({ columns, rows }: any) => {
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          {columns.map((colData: any, index: any) => (
            <th key={colData.key}>{colData.label}</th>
          ))}
        </tr>
      </thead>
      <tbody>
        {rows.map((rowData: any, index: any) => (
          <tr>
            {columns.map((colData: any) => {
              const cellValue = rowData[colData.key];
              return <td> {cellValue}</td>;
            })}
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default DataTable;
