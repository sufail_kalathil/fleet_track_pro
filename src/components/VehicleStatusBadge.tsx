import { Badge } from "react-bootstrap";
import { FC } from "react";
import { VehicleStatusEnum } from "../common";

interface IStatusBadgeProps {
  status: VehicleStatusEnum;
}
export const VehicleStatusBadge: FC<IStatusBadgeProps> = ({ status }) => {
  let bgColor = "primary";
  switch (status) {
    case VehicleStatusEnum.ACTIVE:
      bgColor = "primary";
      break;
    case VehicleStatusEnum.MAINTENANCE:
      bgColor = "warning";
      break;
    case VehicleStatusEnum.RUNNING:
      bgColor = "success";
      break;
    default:
      break;
  }

  return <Badge bg={bgColor}>{status}</Badge>;
};
