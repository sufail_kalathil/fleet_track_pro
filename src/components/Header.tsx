import { Container, Navbar } from "react-bootstrap";
import { FC } from "react";

export const Header: FC = () => {
  return (
    <Navbar
      expand="lg"
      className="bg-body-tertiary shadow-sm p-3 mb-5 bg-white rounded"
    >
      <Container>
        <Navbar.Brand href="#home" className="text-black ">
          Fleet Tracker Pro
        </Navbar.Brand>
      </Container>
    </Navbar>
  );
};
