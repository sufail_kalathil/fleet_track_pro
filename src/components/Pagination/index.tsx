import React, { useEffect, useState } from "react";
import classNames from "classnames";

import "./pagination.css";
import { Button } from "react-bootstrap";

const Pagination = (props: {
  onNavigate: (e: number) => void;
  wrapperClass?: string;
  totalLength: number;
  currentSelection: number;
  countsPerPage: number;
  pageCount?: number;
  pageTextSingular?: string;
  pageTextplural?: string;
}) => {
  const onNavigate =
    (selectedIndex: number) => (event: { preventDefault: () => void }) => {
      event.preventDefault();
      props.onNavigate(selectedIndex);
    };
  const buildPagination = (currentSelection: number, countsPerPage: number) => {
    const { totalLength } = props;
    const start = (currentSelection - 1) * countsPerPage + 1;
    const limit = currentSelection * countsPerPage;
    return (
      <>
        <strong>
          {start} - {limit > totalLength ? totalLength : limit}{" "}
        </strong>
        of <strong>{totalLength}</strong>
      </>
    );
  };
  const [pages, setPages] = useState<(string | number)[] | []>([]);
  const { currentSelection, totalLength, countsPerPage } = props;
  const pageCount = props.pageCount || Math.ceil(totalLength / countsPerPage);

  const range = (start: number, end: number) => {
    const length = end - start + 1;
    return Array.from({ length }, (_, idx) => idx + start);
  };

  const getPages = () => {
    const firstPageIndex = 1;
    const lastPageIndex = pageCount;
    if (pageCount <= 7) {
      return setPages(range(1, lastPageIndex));
    } else if (currentSelection < 5) {
      const leftItemCount = 5;
      const leftRange = range(1, leftItemCount);
      return setPages([...leftRange, "DOTS", lastPageIndex]);
    } else if (currentSelection > pageCount - 4) {
      const rightItemCount = 5;
      const rightRange = range(lastPageIndex - rightItemCount + 1, pageCount);
      return setPages([firstPageIndex, "DOTS", ...rightRange]);
    } else {
      const middleRange = range(currentSelection - 1, currentSelection + 1);
      return setPages([
        firstPageIndex,
        "DOTS",
        ...middleRange,
        "DOTS",
        lastPageIndex,
      ]);
    }
  };

  useEffect(() => {
    getPages();
  }, [currentSelection, pageCount]);

  const classes = classNames("pagination", {
    [`${props.wrapperClass}`]: props.wrapperClass,
  });
  const prev = "<";
  const next = ">";
  if (totalLength) {
    return (
      <div className={classes}>
        <div className={"text"} data-testid="paginationText">
          {"Showing"} {buildPagination(currentSelection, countsPerPage)}{" "}
          {totalLength == 1
            ? props?.pageTextSingular || "entry"
            : props?.pageTextplural || "entries"}
        </div>
        <div className={"number"}>
          <Button
            onClick={() => onNavigate(currentSelection - 1)}
            disabled={currentSelection === 1 || totalLength < countsPerPage}
          >
            {prev}
          </Button>
          {pages?.map((page) => {
            return page !== "DOTS" && typeof page == "number" ? (
              <Button
                key={page}
                className={`${page === currentSelection ? "selected" : ""}`}
                onClick={onNavigate(page)}
              >
                {page?.toString()}
              </Button>
            ) : (
              <span key={"dots"}> ... </span>
            );
          })}
          <Button
            onClick={onNavigate(currentSelection + 1)}
            disabled={
              currentSelection === pageCount || totalLength < countsPerPage
            }
          >
            {next}
          </Button>
        </div>
      </div>
    );
  }
  return null;
};
Pagination.defaultProps = {
  countsPerPage: 10,
  currentSelection: 1,
};

export default Pagination;
