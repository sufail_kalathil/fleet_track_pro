import { Container, Nav, Navbar } from "react-bootstrap";
import "./sidebar.css";

export const Sidebar = () => {
  return (
    <Navbar sticky="top" expand="md" className="sidebar">
      <Container className="sub">
        <Nav className="navbar">
          <Nav.Item
            className={`${window.location.pathname === "/" || window.location.pathname.endsWith("create") ? "active" : ""} item`}
          >
            <Nav.Link href="/">Vehicles</Nav.Link>
          </Nav.Item>
          <Nav.Item
            className={`${window.location.pathname === "/maintenence" ? "active" : ""} item`}
          >
            <Nav.Link href="/maintenance">Maintenance</Nav.Link>
          </Nav.Item>
        </Nav>
      </Container>
    </Navbar>
  );
};
