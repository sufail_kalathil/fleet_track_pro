import {Container} from "react-bootstrap";

export const Footer = () => {
  return(
      <footer>
          <Container fluid className="bg-dark text-light text-center py-3">
              © {new Date().getFullYear()} Fleet Tracker Pro. All rights reserved.
          </Container>
      </footer>
  )
}