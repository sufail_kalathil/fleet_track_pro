import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Container, Spinner } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { fetchVehicleListData } from "../../store/Vehicle/VehicleListing/actions";
import { DataTable, VehicleStatusBadge } from "../../components";
import Pagination from "../../components/Pagination";

export const VehicleListing = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchVehicleListData({ paginate: true, pageSize: 8 }));
  }, []);

  const data = useSelector((state: any) => state.vehicleList);

  return (
    <Container>
      <div className="d-flex justify-content-between align-items-center">
        <h5>Vehicles</h5>
        <div className="m-3 d-flex justify-content-end">
          <Button onClick={() => navigate("/vehicle/create")}>Register</Button>
        </div>
      </div>
      {data.isLoading ? (
        <Spinner></Spinner>
      ) : (
        <>
          <DataTable
            rows={
              data?.vehicleList?.result?.map((i: any) => ({
                ...i,
                action: (
                  <Button onClick={() => navigate(`/vehicle/${i._id}/details`)}>
                    View
                  </Button>
                ),
                status: <VehicleStatusBadge status={i.status} />,
              })) || []
            }
            columns={[
              { key: "model", label: "Model" },
              { key: "brand", label: "Brand" },
              { key: "color", label: "Color" },
              { key: "year", label: "Year" },
              { key: "type", label: "Type" },
              { key: "uin", label: "Registration No" },
              { key: "status", label: "Status" },
              { key: "action", label: "Action" },
            ]}
          />
          <Pagination
            countsPerPage={data?.vehicleList?.pagination?.pageSize}
            totalLength={data?.vehicleList?.pagination?.totalItems}
            currentSelection={data?.vehicleList?.pagination?.pageNo}
            onNavigate={(value) => {
              if (data?.vehicleList?.pagination?.pageNo !== value) {
                dispatch(
                  fetchVehicleListData({
                    pageSize: data?.vehicleList?.pagination?.pageSize,
                    pageNo: value,
                  }),
                );
              }
            }}
            pageCount={data?.vehicleList?.pagination?.totalPages}
          />
        </>
      )}
    </Container>
  );
};
