import { useParams } from "react-router-dom";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchVehicleDetails,
  updateVehicleDetails,
} from "../../store/Vehicle/VehicleDetails/actions";
import { VehicleStatusBadge } from "../../components";
import { VehicleStatusEnum } from "../../common";
import io from "socket.io-client";
import {
  getMaintenanceCostDetails,
  getVehicleAnalytics,
} from "../../store/Analytics/actions";

export const VehicleDetails = () => {
  let { vehicleId } = useParams();
  const dispatch = useDispatch();

  const socketInstance = io("http://localhost:8080");

  // load vehicle details
  useEffect(() => {
    dispatch(fetchVehicleDetails(vehicleId));
    dispatch(getVehicleAnalytics(vehicleId));
    dispatch(getMaintenanceCostDetails(vehicleId));

    // listen for events emitted by the server

    socketInstance.on("connect", () => {
      // alert("Connected to server");
      console.log("Connected to server");
    });

    socketInstance.on("trackingLog", (data) => handleTrackLogEvent(data));

    socketInstance.on("startEvent", (data) => handleStartOrStopEvent(data));

    socketInstance.on("stopEvent", (data) => handleStartOrStopEvent(data));

    return () => {
      socketInstance.disconnect();
    };
  }, []);

  const findAnalytics = (analytics: any) => {
    if (!analytics) {
      return;
    }
    const { maintenanceCost, vehicleAnalytics } = analytics;
    if (maintenanceCost.length) {
      totalServiceCost = maintenanceCost[0].totalCost;
    }
    if (vehicleAnalytics.length) {
      totalDistance = vehicleAnalytics[0].totalDistance;
      totalDuration = formatDuration(vehicleAnalytics[0].totalDuration);
    }
  };

  const formatDuration = (milliseconds: number): string => {
    let totalSeconds = milliseconds / 1000;
    const hours = Math.floor(totalSeconds / 3600);
    totalSeconds %= 3600;
    const minutes = Math.floor(totalSeconds / 60);
    const seconds = Math.floor(totalSeconds % 60);
    return `${hours} hours ${minutes} minutes ${seconds} seconds`;
  };

  const data = useSelector((state: any) => state.vehicleDetails);
  const analytics = useSelector((state: any) => state.analytics);

  let totalServiceCost = 0;
  let totalDistance = 0;
  let totalDuration = "0";

  findAnalytics(analytics);
  const { isLoading, vehicle } = data;

  const handleTrackLogEvent = (_data: any) => {
    if (!_data) {
      return;
    }

    try {
      const parsedData = JSON.parse(_data);
      if (parsedData.vehicleId !== vehicleId) {
        return;
      }
      dispatch(updateVehicleDetails(parsedData));
    } catch (error) {
      console.log("Error parsing JSON", error);
    }
  };

  const handleStartOrStopEvent = (_data: any) => {
    if (!_data) {
      return;
    }

    try {
      const parsedData = JSON.parse(_data);
      if (parsedData.vehicleId !== vehicleId) {
        return;
      }
      dispatch(updateVehicleDetails(parsedData));
    } catch (error) {
      console.log("Error parsing JSON", error);
    }
  };

  return (
    <div>
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card-header p-3 d-flex justify-content-between align-items-center">
                <div className="d-flex align-items-center">
                  <h5 className="card-title m-0">Vehicle Details</h5>
                </div>
                <div>
                  <span className="badge badge-success">
                    <VehicleStatusBadge
                      status={vehicle.status as VehicleStatusEnum}
                    />
                  </span>
                </div>
              </div>
              <div className="card-body">
                <section className="row">
                  <div className="col-12">
                    <div className="row">
                      <div className="col-6">
                        <h6>Vehicle Brand:</h6>
                        <p>{vehicle.brand}</p>
                      </div>
                      <div className="col-6">
                        <h6>Vehicle Model:</h6>
                        <p>{vehicle.model}</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-6">
                        <h6>Vehicle Year:</h6>
                        <p>{vehicle.year}</p>
                      </div>
                      <div className="col-6">
                        <h6>Registration No:</h6>
                        <p>{vehicle.uin}</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-6">
                        <h6>Color:</h6>
                        <p>{vehicle.color}</p>
                      </div>
                      <div className="col-6">
                        <h6>Type:</h6>
                        <p>{vehicle.type}</p>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container my-5">
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card-header p-3 d-flex justify-content-between align-items-center">
                <div className="d-flex align-items-center">
                  <h5 className="card-title m-0">Live Analytics</h5>
                </div>
              </div>
              <div className="card-body">
                <section className="row">
                  <div className="col-12">
                    <div className="row">
                      <div className="col-4">
                        <h6>Total Service Cost:</h6>
                        <p>{totalServiceCost}</p>
                      </div>
                      <div className="col-4">
                        <h6>Total Distance Covered:</h6>
                        <p>{totalDistance ?? "--"}</p>
                      </div>
                      <div className="col-4">
                        <h6>Total Duration:</h6>
                        <p>{totalDuration ?? "--"}</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-6">
                        <h6>Current Location:</h6>
                        <p>{vehicle.locationName ?? "--"}</p>
                      </div>
                      <div className="col-6">
                        <h6>Speed:</h6>
                        <p>{vehicle.speed ?? "0"} Km</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-6">
                        <h6>Lat:</h6>
                        <p>{vehicle.latitude ?? "--"}</p>
                      </div>
                      <div className="col-6">
                        <h6>Long:</h6>
                        <p>{vehicle.longitude ?? "--"}</p>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
