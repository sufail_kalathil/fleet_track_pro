import "bootstrap/dist/css/bootstrap.min.css";
import { ErrorMessage, Field, Formik, FormikHelpers, Form } from "formik";
import * as Yup from "yup";
import { IVehicleRegistrationFormValues } from "../../common/abstraction/vehicle-registration-form-values";
import { brands } from "../../constants/brands";
import { IBrand, IVehicleType } from "../../common";
import { vehicleType } from "../../constants/vehicleType";
import { useDispatch } from "react-redux";
import { addVehicle } from "../../store/Vehicle/VehicleRegistration/actions";
import { useNavigate } from "react-router-dom";

export const VehicleRegistration = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // formik configuration
  const initialValues: IVehicleRegistrationFormValues = {
    brand: "",
    model: "",
    year: "",
    uin: "",
    color: "",
    type: "",
  };

  const errorMessage = {
    brand: {
      required: "Brand is required",
    },
    model: {
      required: "Model is required",
    },
    year: {
      required: "Year is required",
      type: "should be a number",
    },
    uin: {
      required: "Registration Number is required",
    },
  };

  const validationSchema = Yup.object({
    brand: Yup.string().required(`${errorMessage.brand.required}`),
    model: Yup.string().required(`${errorMessage.model.required}`),
    year: Yup.number()
      .typeError(`${errorMessage.year.type}`)
      .required(`${errorMessage.year.required}`),
    uin: Yup.string().required(`${errorMessage.uin.required}`),
  });

  const onSubmit = (
    values: IVehicleRegistrationFormValues,
    { setSubmitting }: FormikHelpers<IVehicleRegistrationFormValues>,
  ) => {
    dispatch(
      addVehicle({
        values,
        redirect: () => {
          navigate("/");
        },
      }),
    );
    setSubmitting(false);
  };

  return (
    <div className="form">
      <div className="container">
        <div className="d-flex justify-content-between justify-content-start">
          <h5>Vehicle Registration</h5>
        </div>
      </div>

      <div className="container mt-4">
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={onSubmit}
        >
          {({ errors, touched }) => (
            <Form>
              <div className="row">
                <div className="col-lg-6 col-md-12 col-sm-12">
                  <div className="mb-3">
                    <label className="form-label d-block text-start">
                      Brand
                    </label>
                    <Field as="select" name="brand" className="form-control">
                      <option selected={true} value="">
                        --select--
                      </option>
                      {brands.map((option: IBrand) => {
                        return (
                          <option key={option.id} value={option.name}>
                            {option.name}
                          </option>
                        );
                      })}
                    </Field>
                    <ErrorMessage
                      className="error mt-2"
                      name="brand"
                      component="div"
                    />
                  </div>
                </div>
                <div className="col-lg-6 col-md-12 col-sm-12">
                  <div className="mb-3">
                    <label className="form-label d-block text-start ">
                      Model
                    </label>
                    <Field type="text" name="model" className="form-control" />

                    <ErrorMessage
                      className="error mt-2"
                      name="model"
                      component="div"
                    />
                  </div>
                </div>
                <div className="col-lg-6 col-md-12 col-sm-12">
                  <div className="mb-3">
                    <label className="form-label d-block text-start ">
                      Color
                    </label>
                    <Field type="text" name="color" className="form-control" />
                    <ErrorMessage
                      className="error mt-2"
                      name="color"
                      component="div"
                    />
                  </div>
                </div>
                <div className="col-lg-6 col-md-12 col-sm-12">
                  <div className="mb-3">
                    <label className="form-label d-block text-start ">
                      Year
                    </label>
                    <Field type="text" name="year" className="form-control" />
                    <ErrorMessage
                      className="error mt-2"
                      name="year"
                      component="div"
                    />
                  </div>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-12">
                  <div className="mb-3">
                    <label className="form-label  d-block text-start">
                      Type
                    </label>

                    <Field as="select" name="type" className="form-control">
                      <option selected={true} value="">
                        --select--
                      </option>
                      {vehicleType.map((option: IVehicleType) => {
                        return (
                          <option key={option.id} value={option.name}>
                            {option.name}
                          </option>
                        );
                      })}
                    </Field>

                    <ErrorMessage
                      className="error mt-2"
                      name="type"
                      component="div"
                    />
                  </div>
                </div>
                <div className="col-lg-6 col-md-6 col-sm-12">
                  <div className="mb-3">
                    <label className="form-label  d-block text-start">
                      Registration No.
                    </label>
                    <Field type="text" name="uin" className="form-control" />
                    <ErrorMessage
                      className="error mt-2"
                      name="uin"
                      component="div"
                    />
                  </div>
                </div>
              </div>
              <button className="btn btn-primary d-table ms-auto" type="submit">
                Save
              </button>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};
