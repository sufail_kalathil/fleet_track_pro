import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Container, Spinner } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { DataTable, VehicleStatusBadge } from "../../components";
import { fetchMaintenanceListData } from "../../store/Maintenance/MaintenanceListing/actions";
import Pagination from "../../components/Pagination";
import { fetchVehicleListData } from "../../store/Vehicle/VehicleListing/actions";

export const MaintenanceListing = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchMaintenanceListData({ paginate: true, pageSize: 2 }));
  }, []);

  const data = useSelector((state: any) => state.maintenanceList);

  return (
    <Container>
      <div className="d-flex justify-content-between align-items-center">
        <h5>Maintenance</h5>
        <div className="m-3 d-flex justify-content-end">
          <Button onClick={() => navigate("/maintenance/add")}>Add</Button>
        </div>
      </div>
      {data.isLoading ? (
        <Spinner></Spinner>
      ) : (
        <>
          <DataTable
            rows={
              data?.maintenanceList?.result?.map((i: any) => ({
                ...i,
                vehicleId: `${i.vehicleId.uin}:${i.vehicleId.brand}, ${i.vehicleId.model}`,
                action: <Button>Edit</Button>,
              })) || []
            }
            columns={[
              { key: "vehicleId", label: "Vehicle" },
              { key: "serviceDate", label: "Date" },
              { key: "serviceType", label: "Service Type" },
              { key: "serviceProvider", label: "Service Provider" },
              { key: "serviceCost", label: "Cost" },
              { key: "action", label: "Action" },
            ]}
          />
          <Pagination
            countsPerPage={data?.maintenanceList?.pagination?.pageSize}
            totalLength={data?.maintenanceList?.pagination?.totalItems}
            currentSelection={data?.maintenanceList?.pagination?.pageNo}
            onNavigate={(value) => {
              if (data?.maintenanceList?.pagination?.pageNo !== value) {
                dispatch(
                  fetchMaintenanceListData({
                    pageSize: data?.maintenanceList?.pagination?.pageSize,
                    pageNo: value,
                  }),
                );
              }
            }}
            pageCount={data?.maintenanceList?.pagination?.totalPages}
          />
        </>
      )}
    </Container>
  );
};
