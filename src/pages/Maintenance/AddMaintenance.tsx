import React, { useEffect } from "react";
import * as Yup from "yup";
import { ErrorMessage, Field, Formik, FormikHelpers, Form } from "formik";
import { fetchVehicleListData } from "../../store/Vehicle/VehicleListing/actions";
import { useDispatch, useSelector } from "react-redux";
import { IMaintenanceFormValues } from "../../common";
import { addMaintenance } from "../../store/Maintenance/AddMaintenance/actions";
import { useNavigate } from "react-router-dom";

export const AddMaintenance = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // load vehicles

  useEffect(() => {
    dispatch(fetchVehicleListData({ paginate: false }));
  }, []);

  const data = useSelector((state: any) => state.vehicleList);

  // Formik configuration

  const initialValues: IMaintenanceFormValues = {
    vehicleId: "",
    serviceDate: "",
    serviceType: "",
    serviceProvider: "",
    serviceCost: 0,
    notes: "",
  };

  const errorMessage = {
    vehicleId: {
      required: "vehicleId is required",
    },
    serviceDate: {
      required: "serviceDate is required",
    },
    serviceType: {
      required: "serviceType is required",
    },
    serviceProvider: {
      required: "serviceProvider is required",
    },
    serviceCost: {
      required: "serviceCost is required",
      type: "should be a number",
    },
  };

  const validationSchema = Yup.object({
    vehicleId: Yup.string().required(`${errorMessage.vehicleId.required}`),
    serviceDate: Yup.string().required(`${errorMessage.serviceDate.required}`),
    serviceType: Yup.string().required(`${errorMessage.serviceType.required}`),
    serviceProvider: Yup.string().required(
      `${errorMessage.serviceProvider.required}`,
    ),
    serviceCost: Yup.number()
      .typeError(`${errorMessage.serviceCost.type}`)
      .required(`${errorMessage.serviceCost.required}`),
  });

  const onSubmit = (
    values: IMaintenanceFormValues,
    { setSubmitting }: FormikHelpers<IMaintenanceFormValues>,
  ) => {
    dispatch(
      addMaintenance({
        values,
        redirect: () => {
          navigate("/maintenance");
        },
      }),
    );
    setSubmitting(false);
  };

  return (
    <div className="form">
      <div className="container">
        <div className="d-flex justify-content-between justify-content-start">
          <h5>Maintenance</h5>
        </div>
      </div>

      <div className="container mt-4">
        <Formik
          enableReinitialize={true}
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={onSubmit}
        >
          {({ errors, touched }) => (
            <Form>
              <div className="row">
                <div className="col-lg-6 col-md-12 col-sm-12">
                  <div className="mb-3">
                    <label className="form-label d-block text-start">
                      Vehicles
                    </label>
                    <Field
                      as="select"
                      name="vehicleId"
                      className="form-control"
                    >
                      <option selected={true} value="">
                        --select--
                      </option>
                      {data.vehicleList.map((option: any) => {
                        return (
                          <option key={option._id} value={option._id}>
                            {option.uin}
                          </option>
                        );
                      })}
                    </Field>
                    <ErrorMessage
                      className="error mt-2"
                      name="vehicleId"
                      component="div"
                    />
                  </div>
                </div>
                <div className="col-lg-6 col-md-12 col-sm-12">
                  <div className="mb-3">
                    <label className="form-label d-block text-start ">
                      Service type
                    </label>
                    <Field
                      type="text"
                      name="serviceType"
                      className="form-control"
                    />

                    <ErrorMessage
                      className="error mt-2"
                      name="serviceType"
                      component="div"
                    />
                  </div>
                </div>
                <div className="col-lg-6 col-md-12 col-sm-12">
                  <div className="mb-3">
                    <label className="form-label d-block text-start ">
                      Service Provider
                    </label>
                    <Field
                      type="text"
                      name="serviceProvider"
                      className="form-control"
                    />
                    <ErrorMessage
                      className="error mt-2"
                      name="serviceProvider"
                      component="div"
                    />
                  </div>
                </div>
                <div className="col-lg-6 col-md-12 col-sm-12">
                  <div className="mb-3">
                    <label className="form-label d-block text-start ">
                      Service Cost
                    </label>
                    <Field
                      type="text"
                      name="serviceCost"
                      className="form-control"
                    />
                    <ErrorMessage
                      className="error mt-2"
                      name="serviceCost"
                      component="div"
                    />
                  </div>
                </div>
                <div className="col-lg-4 col-md-6 col-sm-12">
                  <div className="mb-3">
                    <label className="form-label  d-block text-start">
                      Service Date
                    </label>

                    <Field
                      name="serviceDate"
                      type="date"
                      className="form-control"
                    />

                    <ErrorMessage
                      className="error mt-2"
                      name="serviceDate"
                      component="div"
                    />
                  </div>
                </div>
                <div className="col-lg-8 col-md-6 col-sm-12">
                  <div className="mb-3">
                    <label className="form-label">Notes</label>
                    <input type="text" name="notes" className="form-control" />
                  </div>
                </div>
              </div>
              <button className="btn btn-primary d-table ms-auto" type="submit">
                Save
              </button>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};
