import { call, put, takeLatest } from "redux-saga/effects";
import {
  fetchVehicleListDataFailed,
  fetchVehicleListDataSuccess,
} from "../store/Vehicle/VehicleListing/actions";
import { FETCH_VEHICLE_LIST } from "../store/Vehicle/VehicleListing/constants";
import {
  getVehicleAnalytics,
  getVehicleMaintenanceCost,
} from "../endpoints/analytics";
import {
  fetchMaintenanceListDataFailed,
  fetchMaintenanceListDataSuccess,
} from "../store/Maintenance/MaintenanceListing/actions";
import {
  FETCH_MAINTENANCE_COST,
  FETCH_VEHICLE_ANALYTICS,
} from "../store/Analytics/constants";
import {
  getMaintenanceCostDetailsFailed,
  getMaintenanceCostDetailsSuccess,
  getVehicleAnalyticsFailed,
  getVehicleAnalyticsSuccess,
} from "../store/Analytics/actions";

export function* getMaintenanceCostSaga(action: any): any {
  try {
    const response: any = yield call(getVehicleMaintenanceCost, action.payload);
    yield put(getMaintenanceCostDetailsSuccess(response));
  } catch (err: any) {
    yield put(getMaintenanceCostDetailsFailed());
    console.log("err", err);
  }
}

function* getMaintenanceCostSagaWatcher() {
  yield takeLatest(FETCH_MAINTENANCE_COST, getMaintenanceCostSaga);
}

export function* getVehicleAnalyticsSaga(action: any): any {
  try {
    const response: any = yield call(getVehicleAnalytics, action.payload);
    yield put(getVehicleAnalyticsSuccess(response));
  } catch (err: any) {
    yield put(getVehicleAnalyticsFailed());
    console.log("err", err);
  }
}

function* getVehicleAnalyticsSagaWatcher() {
  yield takeLatest(FETCH_VEHICLE_ANALYTICS, getVehicleAnalyticsSaga);
}

export const analyticsSagaWatcherList = [
  getMaintenanceCostSagaWatcher,
  getVehicleAnalyticsSagaWatcher,
];
