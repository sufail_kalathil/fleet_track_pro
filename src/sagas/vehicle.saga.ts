import { call, put, takeLatest } from "redux-saga/effects";
import {
  addVehicle,
  getVehicleById,
  getVehicleList,
} from "../endpoints/vehicle";
import {
  fetchVehicleListDataFailed,
  fetchVehicleListDataSuccess,
} from "../store/Vehicle/VehicleListing/actions";
import { FETCH_VEHICLE_LIST } from "../store/Vehicle/VehicleListing/constants";
import {
  addVehicleFailed,
  addVehicleSuccess,
} from "../store/Vehicle/VehicleRegistration/actions";
import { ADD_VEHICLE } from "../store/Vehicle/VehicleRegistration/constants";
import {
  fetchVehicleDetails,
  fetchVehicleDetailsFailed,
  fetchVehicleDetailsSuccess,
} from "../store/Vehicle/VehicleDetails/actions";
import { FETCH_VEHICLE_DETAILS } from "../store/Vehicle/VehicleDetails/constants";
import { showFailToast, showSuccessToast } from "../store/Toast/actions";

export function* getVehicleListSaga(action: any): any {
  try {
    const response: any = yield call(getVehicleList, action.payload);
    yield put(fetchVehicleListDataSuccess(response));
  } catch (err: any) {
    yield put(fetchVehicleListDataFailed());
    console.log("err", err);
  }
}

export function* addVehicleSaga(action: any): any {
  try {
    const response: any = yield call(addVehicle, action.payload.values);
    yield put(addVehicleSuccess(response));
    yield put(showSuccessToast());
    yield put(action.payload.redirect());
  } catch (err: any) {
    yield put(addVehicleFailed());
    yield put(showFailToast("Failed to add vehicle"));
    console.log("err", err);
  }
}

export function* getVehicleDetailsSaga(action: any): any {
  try {
    const response: any = yield call(getVehicleById, action.payload);
    yield put(fetchVehicleDetailsSuccess(response));
  } catch (err: any) {
    yield put(fetchVehicleDetailsFailed());
    console.log("err", err);
    alert("error");
  }
}

function* vehicleListSagaWatcher() {
  yield takeLatest(FETCH_VEHICLE_LIST, getVehicleListSaga);
}

function* addVehicleSagaWatcher() {
  yield takeLatest(ADD_VEHICLE, addVehicleSaga);
}

function* vehicleDetailsSagaWatcher() {
  yield takeLatest(FETCH_VEHICLE_DETAILS, getVehicleDetailsSaga);
}
export const vehicleSagaWatcherList = [
  vehicleListSagaWatcher,
  addVehicleSagaWatcher,
  vehicleDetailsSagaWatcher,
];
