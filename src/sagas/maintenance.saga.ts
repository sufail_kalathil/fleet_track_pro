import { call, put, takeLatest } from "redux-saga/effects";
import { getMaintenanceList, addMaintenance } from "../endpoints/maintenance";
import { FETCH_MAINTENANCE_LIST } from "../store/Maintenance/MaintenanceListing/constants";
import {
  fetchMaintenanceListDataFailed,
  fetchMaintenanceListDataSuccess,
} from "../store/Maintenance/MaintenanceListing/actions";
import { ADD_MAINTENANCE } from "../store/Maintenance/AddMaintenance/constants";
import {
  addMaintenanceFailed,
  addMaintenanceSuccess,
} from "../store/Maintenance/AddMaintenance/actions";
import { showFailToast, showSuccessToast } from "../store/Toast/actions";

export function* getMaintenanceListSaga(action: any): any {
  try {
    const response: any = yield call(getMaintenanceList, action.payload);
    yield put(fetchMaintenanceListDataSuccess(response));
  } catch (err: any) {
    yield put(fetchMaintenanceListDataFailed);
    console.log("err", err);
    alert("error");
  }
}

export function* addMaintenanceSaga(action: any): any {
  try {
    const response: any = yield call(addMaintenance, action.payload.values);
    yield put(addMaintenanceSuccess(response));
    yield put(showSuccessToast());
    yield put(action.payload.redirect());
  } catch (err: any) {
    yield put(addMaintenanceFailed());
    yield put(showFailToast("Failed"));

    console.log("err", err);
  }
}

function* addMaintenanceSagaWatcher() {
  yield takeLatest(ADD_MAINTENANCE, addMaintenanceSaga);
}

function* maintenanceListSagaWatcher() {
  yield takeLatest(FETCH_MAINTENANCE_LIST, getMaintenanceListSaga);
}

export const maintenanceListSagaWatcherList = [
  maintenanceListSagaWatcher,
  addMaintenanceSagaWatcher,
];
