import { all, fork } from "redux-saga/effects";
import { vehicleSagaWatcherList } from "./vehicle.saga";
import { maintenanceListSagaWatcherList } from "./maintenance.saga";
import { analyticsSagaWatcherList } from "./analytics.saga";

export default function* rootSaga() {
  const sagas = [
    ...vehicleSagaWatcherList,
    ...maintenanceListSagaWatcherList,
    ...analyticsSagaWatcherList,
  ].map((x) => fork(x));
  yield all(sagas);
}
