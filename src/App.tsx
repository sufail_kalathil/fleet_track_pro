import React, { useState } from "react";
import "./App.css";
import { Footer, Header, Sidebar, ToastBar } from "./components";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import {
  AddMaintenance,
  VehicleDetails,
  VehicleListing,
  VehicleRegistration,
} from "./pages";
import { MaintenanceListing } from "./pages";
import { Button, Col, Row, Toast } from "react-bootstrap";

function App() {
  const [show, setShow] = useState(true);

  return (
    <div className="App">
      <BrowserRouter>
        {/* eslint-disable-next-line react/jsx-no-undef */}
        <Sidebar />
        <div className="w-100">
          <Header />
          <div className="section p-3">
            <ToastBar />
            <Routes>
              {/* eslint-disable-next-line react/jsx-no-undef */}
              <Route path="/" element={<VehicleListing />} />
              <Route path="/vehicle/create" element={<VehicleRegistration />} />
              <Route
                path="/vehicle/:vehicleId/details"
                element={<VehicleDetails />}
              />
              <Route path="/maintenance/add" element={<AddMaintenance />} />
              <Route path="/maintenance" element={<MaintenanceListing />} />
            </Routes>
          </div>
          {/* <Footer /> */}
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
