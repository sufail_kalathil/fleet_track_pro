export interface IParams {
  [key: string]: string;
}

export interface IHttpRequest {
  requestType: "GET" | "POST" | "PATCH" | "DELETE" | "PUT";
  url: string;
  params?: IParams;
  body?: any;
}
