export interface IVehicleType {
  id: number;
  name: string;
}
