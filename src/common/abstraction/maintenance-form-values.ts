export interface IMaintenanceFormValues {
  vehicleId: string;
  serviceDate: any;
  serviceType: string;
  serviceProvider: string;
  serviceCost: number;
  notes: string;
}
