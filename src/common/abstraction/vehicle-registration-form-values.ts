export interface IVehicleRegistrationFormValues {
  brand: string;
  model: string;
  year: string;
  uin: string;
  color: string;
  type: string;
}
