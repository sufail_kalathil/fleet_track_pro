import { VehicleStatusEnum } from "../enum/vehicle-status.enum";

export interface IVehicle {
  _id: string;
  brand: string;
  model: string;
  year?: string;
  uin?: string;
  color?: string;
  type?: string;
  engineNumber?: string;
  status?: VehicleStatusEnum;
  isActive?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}
