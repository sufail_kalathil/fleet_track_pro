import { AnyAction } from "redux";

import * as actionTypes from "./constants";

export const initialValue: any = {
  show: false,
  type: "success",
  title: "Success",
  message: "",
};

const toastHandleReducer = (state = initialValue, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.SHOW_TOAST_SUCCESS:
      return {
        ...state,
        type: "success",
        show: true,
        title: "Success",
        message: action.payload,
      };
    case actionTypes.SHOW_TOAST_FAIL: {
      return {
        ...state,
        show: false,
        type: "error",
        title: "Error",
        message: action.payload,
      };
    }
    case actionTypes.CLOSE_TOAST:
      return {
        ...state,
        show: false,
      };
    default:
      return state;
  }
};

export default toastHandleReducer;
