import * as actionType from "./constants";

export const showSuccessToast = (payload?: any) => {
  return {
    type: actionType.SHOW_TOAST_SUCCESS,
    payload,
  };
};

export const showFailToast = (message: string) => {
  return {
    type: actionType.SHOW_TOAST_FAIL,
    payload: message,
  };
};

export const closeToast = (data?: any) => {
  return {
    type: actionType.CLOSE_TOAST,
    payload: data,
  };
};
