import { AnyAction } from "redux";

import * as actionTypes from "./constants";

export const initialValue: any = {
  maintenanceCost: [],
  vehicleAnalytics: [],
  isLoading: false,
};

const AnalyticsReducer = (state = initialValue, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.FETCH_VEHICLE_ANALYTICS:
      return { ...state, isLoading: true };
    case actionTypes.FETCH_VEHICLE_ANALYTICS_SUCCESS: {
      return {
        ...state,
        vehicleAnalytics: action.payload || [],
        isLoading: false,
      };
    }
    case actionTypes.FETCH_VEHICLE_ANALYTICS_FAILED:
      return { ...state, isLoading: false };

    case actionTypes.FETCH_MAINTENANCE_COST:
      return { ...state, isLoading: true };
    case actionTypes.FETCH_MAINTENANCE_COST_SUCCESS: {
      return {
        ...state,
        maintenanceCost: action.payload || [],
        isLoading: false,
      };
    }
    case actionTypes.FETCH_MAINTENANCE_COST_FAILED:
      return { ...state, isLoading: false };

    default:
      return state;
  }
};

export default AnalyticsReducer;
