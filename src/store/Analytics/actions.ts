import * as actionType from "./constants";

export const getMaintenanceCostDetails = (payload: any) => {
  return {
    type: actionType.FETCH_MAINTENANCE_COST,
    payload,
  };
};

export const getMaintenanceCostDetailsSuccess = (data: any) => {
  return {
    type: actionType.FETCH_MAINTENANCE_COST_SUCCESS,
    payload: data,
  };
};

export const getMaintenanceCostDetailsFailed = () => {
  return {
    type: actionType.FETCH_MAINTENANCE_COST_FAILED,
  };
};

export const getVehicleAnalytics = (payload: any) => {
  return {
    type: actionType.FETCH_VEHICLE_ANALYTICS,
    payload,
  };
};

export const getVehicleAnalyticsSuccess = (data: any) => {
  return {
    type: actionType.FETCH_VEHICLE_ANALYTICS_SUCCESS,
    payload: data,
  };
};

export const getVehicleAnalyticsFailed = () => {
  return {
    type: actionType.FETCH_VEHICLE_ANALYTICS_FAILED,
  };
};
