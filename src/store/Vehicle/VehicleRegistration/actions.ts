import * as actionType from "./constants";

export const addVehicle = (payload: any) => {
  return {
    type: actionType.ADD_VEHICLE,
    payload,
  };
};

export const addVehicleSuccess = (data: any) => {
  return {
    type: actionType.ADD_VEHICLE_SUCCESS,
    payload: data,
  };
};

export const addVehicleFailed = () => {
  return {
    type: actionType.ADD_VEHICLE_FAILED,
  };
};
