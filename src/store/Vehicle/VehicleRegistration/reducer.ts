import { AnyAction } from "redux";

import * as actionTypes from "./constants";

export const initialValue: any = {
  isLoading: false,
};

const addVehicleReducer = (state = initialValue, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.ADD_VEHICLE:
      return { ...state, isLoading: true };
    case actionTypes.ADD_VEHICLE_SUCCESS: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case actionTypes.ADD_VEHICLE_FAILED:
      return { ...state, isLoading: false };

    default:
      return state;
  }
};

export default addVehicleReducer;
