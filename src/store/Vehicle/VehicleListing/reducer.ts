import { AnyAction } from "redux";

import * as actionTypes from "./constants";

export const initialValue: any = {
  vehicleList: [],
  isLoading: false,
};

const vehicleListReducer = (state = initialValue, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.FETCH_VEHICLE_LIST:
      return { ...state, isLoading: true };
    case actionTypes.FETCH_VEHICLE_LIST_SUCCESS: {
      return {
        ...state,
        vehicleList: action.payload || [],
        isLoading: false,
      };
    }
    case actionTypes.FETCH_VEHICLE_LIST_FAILED:
      return { ...state, isLoading: false };

    default:
      return state;
  }
};

export default vehicleListReducer;
