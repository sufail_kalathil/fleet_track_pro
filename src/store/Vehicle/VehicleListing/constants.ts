export const FETCH_VEHICLE_LIST = "FETCH_VEHICLE_LIST";
export const FETCH_VEHICLE_LIST_SUCCESS = "FETCH_VEHICLE_LIST_SUCCESS";
export const FETCH_VEHICLE_LIST_FAILED = "FETCH_VEHICLE_LIST_FAILED";

export const FETCH_VEHICLE_BY_ID = "FETCH_VEHICLE_BY_ID";
export const FETCH_VEHICLE_BY_ID_SUCCESS = "FETCH_VEHICLE_BY_ID_SUCCESS";
export const FETCH_VEHICLE_BY_ID_FAILED = "FETCH_VEHICLE_BY_ID_FAILED";
