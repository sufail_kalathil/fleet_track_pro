import * as actionType from "./constants";
import { FETCH_VEHICLE_BY_ID_SUCCESS } from "./constants";

export const fetchVehicleListData = (payload: any) => {
  return {
    type: actionType.FETCH_VEHICLE_LIST,
    payload,
  };
};

export const fetchVehicleListDataSuccess = (data: any) => {
  return {
    type: actionType.FETCH_VEHICLE_LIST_SUCCESS,
    payload: data,
  };
};

export const fetchVehicleListDataFailed = () => {
  return {
    type: actionType.FETCH_VEHICLE_LIST_FAILED,
  };
};

export const fetchVehicleById = (payload: any) => {
  return {
    type: actionType.FETCH_VEHICLE_BY_ID,
    payload,
  };
};

export const fetchVehicleByIdSuccess = (data: any) => {
  return {
    type: actionType.FETCH_VEHICLE_BY_ID_SUCCESS,
    payload: data,
  };
};

export const fetchVehicleByIdFailed = () => {
  return {
    type: actionType.FETCH_VEHICLE_BY_ID_FAILED,
  };
};
