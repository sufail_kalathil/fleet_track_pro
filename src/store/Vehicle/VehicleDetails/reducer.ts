import { AnyAction } from "redux";

import * as actionTypes from "./constants";

export const initialValue: any = {
  vehicle: {},
  isLoading: false,
};

const vehicleDetailsReducer = (state = initialValue, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.FETCH_VEHICLE_DETAILS:
      return { ...state, isLoading: true };
    case actionTypes.FETCH_VEHICLE_DETAILS_SUCCESS: {
      return {
        ...state,
        vehicle: action.payload || {},
        isLoading: false,
      };
    }
    case actionTypes.FETCH_VEHICLE_DETAILS_FAILED:
      return { ...state, isLoading: false };

    case actionTypes.UPDATE_VEHICLE_DETAILS:
      return {
        ...state,
        vehicle: {
          ...state.vehicle,
          ...action.payload,
        },
      };

    default:
      return state;
  }
};

export default vehicleDetailsReducer;
