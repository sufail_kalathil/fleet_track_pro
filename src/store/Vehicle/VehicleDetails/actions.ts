import * as actionType from "./constants";

export const fetchVehicleDetails = (payload: any) => {
  return {
    type: actionType.FETCH_VEHICLE_DETAILS,
    payload,
  };
};

export const updateVehicleDetails = (data: any) => {
  return {
    type: actionType.UPDATE_VEHICLE_DETAILS,
    payload: data,
  };
};

export const fetchVehicleDetailsSuccess = (data: any) => {
  return {
    type: actionType.FETCH_VEHICLE_DETAILS_SUCCESS,
    payload: data,
  };
};

export const fetchVehicleDetailsFailed = () => {
  return {
    type: actionType.FETCH_VEHICLE_DETAILS_FAILED,
  };
};
