import { applyMiddleware, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { createLogger } from "redux-logger";
import createSagaMiddleware from "redux-saga";
import rootSaga from "../sagas";

// config
import rootReducer from "./rootReducer";

const sagaMiddleware = createSagaMiddleware();

const logger = (createLogger as any)();

const dev = process.env.NODE_ENV === "development";
let middleware = dev
  ? applyMiddleware(logger, sagaMiddleware)
  : applyMiddleware(sagaMiddleware);

if (dev) {
  middleware = composeWithDevTools(middleware);
}

const store = createStore(rootReducer, middleware);

sagaMiddleware.run(rootSaga);

export default store;
