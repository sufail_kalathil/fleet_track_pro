import * as actionType from "./constants";
import { IMaintenanceFormValues } from "../../../common";

export const addMaintenance = (payload: any) => {
  return {
    type: actionType.ADD_MAINTENANCE,
    payload,
  };
};

export const addMaintenanceSuccess = (data: any) => {
  return {
    type: actionType.ADD_MAINTENANCE_SUCCESS,
    payload: data,
  };
};

export const addMaintenanceFailed = () => {
  return {
    type: actionType.ADD_MAINTENANCE_SUCCESS,
  };
};
