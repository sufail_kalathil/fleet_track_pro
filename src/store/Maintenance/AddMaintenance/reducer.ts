import { AnyAction } from "redux";

import * as actionTypes from "./constants";

export const initialValue: any = {
  isLoading: false,
};

const addMaintenanceReducer = (state = initialValue, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.ADD_MAINTENANCE:
      return { ...state, isLoading: true };
    case actionTypes.ADD_MAINTENANCE_SUCCESS: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case actionTypes.ADD_MAINTENANCE_FAILED:
      return { ...state, isLoading: false };
    default:
      return state;
  }
};

export default addMaintenanceReducer;
