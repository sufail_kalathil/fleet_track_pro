import * as actionType from "./constants";

export const fetchMaintenanceListData = (payload: any) => {
  return {
    type: actionType.FETCH_MAINTENANCE_LIST,
    payload,
  };
};

export const fetchMaintenanceListDataSuccess = (data: any) => {
  return {
    type: actionType.FETCH_MAINTENANCE_LIST_SUCCESS,
    payload: data,
  };
};

export const fetchMaintenanceListDataFailed = () => {
  return {
    type: actionType.FETCH_MAINTENANCE_LIST_FAILED,
  };
};
