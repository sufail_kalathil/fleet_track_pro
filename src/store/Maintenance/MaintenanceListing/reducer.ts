import { AnyAction } from "redux";

import * as actionTypes from "./constants";

export const initialValue: any = {
  maintenanceList: [],
  isLoading: false,
};

const maintenanceListReducer = (state = initialValue, action: AnyAction) => {
  switch (action.type) {
    case actionTypes.FETCH_MAINTENANCE_LIST:
      return { ...state, isLoading: true };
    case actionTypes.FETCH_MAINTENANCE_LIST_SUCCESS: {
      return {
        ...state,
        maintenanceList: action.payload || [],
        isLoading: false,
      };
    }
    case actionTypes.FETCH_MAINTENANCE_LIST_FAILED:
      return { ...state, isLoading: false };
    default:
      return state;
  }
};

export default maintenanceListReducer;
