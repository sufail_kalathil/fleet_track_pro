import { combineReducers } from "redux";
import vehicleListReducer from "./Vehicle/VehicleListing/reducer";
import maintenanceListReducer from "./Maintenance/MaintenanceListing/reducer";
import addMaintenanceReducer from "./Maintenance/AddMaintenance/reducer";
import vehicleDetailsReducer from "./Vehicle/VehicleDetails/reducer";
import toastHandleReducer from "./Toast/reducer";
import AnalyticsReducer from "./Analytics/reducer";

export interface RootState {}

const rootReducer = combineReducers({
  vehicleList: vehicleListReducer,
  maintenanceList: maintenanceListReducer,
  addMaintenance: addMaintenanceReducer,
  vehicleDetails: vehicleDetailsReducer,
  toastHandler: toastHandleReducer,
  analytics: AnalyticsReducer,
});

export type rootReducerType = ReturnType<typeof rootReducer>;

export default rootReducer;
