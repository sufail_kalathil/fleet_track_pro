import { httpClient } from "../util/http-client";
import { IHttpRequest } from "../common";
import { IVehicleRegistrationFormValues } from "../common/abstraction/vehicle-registration-form-values";

export const getVehicleList = (filterParams?: any) => {
  const httpRequest: IHttpRequest = {
    url: "vehicle",
    params: { params: filterParams },
    requestType: "GET",
  };
  return httpClient(httpRequest);
};

export const addVehicle = (requestBody: IVehicleRegistrationFormValues) => {
  const httpRequest: IHttpRequest = {
    url: "vehicle",
    requestType: "POST",
    body: requestBody,
  };
  return httpClient(httpRequest);
};

export const getVehicleById = (vehicleId: string) => {
  const httpRequest: IHttpRequest = {
    url: `vehicle/${vehicleId}`,
    requestType: "GET",
  };
  return httpClient(httpRequest);
};
