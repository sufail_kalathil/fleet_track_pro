import { IHttpRequest } from "../common";
import { httpClient } from "../util/http-client";

export const getVehicleMaintenanceCost = (vehicleId: string) => {
  const httpRequest: IHttpRequest = {
    url: `maintenance/analytics/vehicle/${vehicleId}/cost`,
    requestType: "GET",
  };
  return httpClient(httpRequest);
};

export const getVehicleAnalytics = (vehicleId: string) => {
  const httpRequest: IHttpRequest = {
    url: `vehicle/${vehicleId}/analytics`,
    requestType: "GET",
  };
  return httpClient(httpRequest);
};
