import { httpClient } from "../util/http-client";
import { IHttpRequest, IMaintenanceFormValues } from "../common";

export const getMaintenanceList = (filterParams?: any) => {
  const httpRequest: IHttpRequest = {
    url: "maintenance",
    params: { params: filterParams },
    requestType: "GET",
  };
  return httpClient(httpRequest);
};

export const addMaintenance = (requestBody: IMaintenanceFormValues) => {
  const httpRequest: IHttpRequest = {
    url: "maintenance",
    requestType: "POST",
    body: requestBody,
  };
  return httpClient(httpRequest);
};
