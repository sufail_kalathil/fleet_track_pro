import { IVehicleType } from "../common";

export const vehicleType: IVehicleType[] = [
  { id: 1, name: "Sedan" },
  { id: 2, name: "SUV" },
  { id: 3, name: "Truck" },
  { id: 4, name: "Hatchback" },
  { id: 5, name: "Convertible" },
  { id: 6, name: "Coupe" },
  { id: 7, name: "Minivan" },
  { id: 8, name: "Crossover" },
];
