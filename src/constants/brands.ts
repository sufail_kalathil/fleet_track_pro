import { IBrand } from "../common";

export const brands: IBrand[] = [
  { id: 1, name: "Toyota" },
  { id: 2, name: "Honda" },
  { id: 3, name: "Ford" },
  { id: 4, name: "Chevrolet" },
  { id: 5, name: "Volkswagen" },
  { id: 6, name: "BMW" },
  { id: 7, name: "Mercedes-Benz" },
  { id: 8, name: "Audi" },
  { id: 9, name: "Nissan" },
  { id: 10, name: "Hyundai" },
];
