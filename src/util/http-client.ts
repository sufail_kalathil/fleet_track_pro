import axios from "axios";
import { IHttpRequest, IParams } from "../common";

const baseUrl = "http://localhost:8080/api";

const baseHeaders = {
  Accept: "application/json",
  "Content-Type": "application/json;charset=UTF-8",
};

export const getAPI = async (url: string, params?: any): Promise<any> => {
  const reqConfig = { headers: baseHeaders };
  return await axios({
    ...reqConfig,
    ...params,
    method: "get",
    url: `${baseUrl}/${url}`,
  })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error);
      alert("error");
    });
};

export const postAPI = async (
  url: string,
  data: any,
  params?: IParams,
): Promise<any> => {
  const reqConfig = { headers: baseHeaders };
  return await axios({
    ...reqConfig,
    params,
    method: "post",
    url: `${baseUrl}/${url}`,
    data,
  })
    .then((response) => {
      return response.data;
    })
    .catch((error) => {
      console.log(error);
      alert("error");
    });
};
export const httpClient = (request: IHttpRequest) => {
  const { requestType, url, params, body } = request;
  switch (requestType) {
    case "GET":
      return getAPI(url, params);
    case "POST":
      return postAPI(url, body, params);
    default:
      break;
  }
};
